﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EBazar.Web.Startup))]
namespace EBazar.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
